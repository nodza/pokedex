//
//  Pokemon.swift
//  Pokedex
//
//  Created by Noel Hwande on 1/16/16.
//  Copyright © 2016 Nodza. All rights reserved.
//

import Foundation

class Pokemon {
    
// 1. Create your variables
    var _name: String!
    var _pokedexId: Int!
    
// 2. Create your getters
    var name: String {
        return _name
    }
    
    var pokedexId: Int {
        return _pokedexId
    }
    
// 3. Create an intializer and pass the data into it
    init(name: String, pokedexId: Int) {
        self._name = name
        self._pokedexId = pokedexId
    }
}